# Shelly
## Ajout d'un nouveau Shelly
Voici les différentes étapes pour utiliser un nouveau Shelly et push ses données sur LYSR.

### Configuration
1. La première étape consiste à câbler et alimenter le composant. La documentation de celui-ci se trouve ici : `doc/datasheet.pdf`
2. Une fois alimenté, appuyer pendant 10s sur le bouton du Shelly pour réinitialiser celui-ci.
3. Une fois cela fait, le Shelly va créer un wifi du style `ShellyPro3EM-$$$$$$$$$$$$$$`. Connectez-vous à ce wifi. Il se peut que la connection soit refusée. Cela peut arriver, c'est normal. Une fois connecté rendez-vous sur l'addresse `192.168.33.1`.
4. Vous êtes maintenant sur l'interface du Shelly. Vous pouvez vous connecter à un wifi, ou activer Ethernet. `!ATTENTION!` Il faut toujours brancher et débrancher le câble RJ45 lorsque le Shelly n'est pas alimenté. N'oubliez pas de prendre note de la nouvelle address IP du composant.
   1. `Optionnel` : Il existe une application Shelly sur laquelle il est possible de répertorier les différentes devices. `Web interface`: https://control.shelly.cloud/, `Application Mobile` : https://www.shelly.com/fr/app/shelly-smart-control#app-download-links.
5. Rendez-vous maintenant sur la page `scripts` de l'interface. Créez un nouveau script et insérez le contenue du fichier `main.js`. N'oubliez pas de changer le `shelly_id` dans la première varibable `CONFIG`. L'id est juste un int. Veuillez vérifier sur LYSR que l'id n'est pas déjà utilisé. Sauvegardez le script et activer l'option `Run on start` qui va lancer le script dès que le shelly est alimenté.

### Ajout du dashboard
1. Demander les accès au workspace des Shelly sur LYSR (id 156).
2. Une fois les accès accordés, ajoutez un nouveau dashboard pour le nouveau Shelly. Pour cela, lancez le script `generate_dashboard` suivi de votre id de shelly. L'id doit correspondre à celui placé dans le script js ! Cela va générer le dashboard en json. Ensuite, faites un post sur la route `/156/dashboards` avec le fichier `dashboard.txt`en tant que body.

## Création des endpoints LYSR
Si par hasard, vous voulez créer un workspace LYSR uniquement pour vous, voici les deux requêtes à faire pour créer les deux enpoints. Faire la requête sur `/156/endpoints`.
### Body du endpoint Power
```
{
    "name": "Power",
    "collectionName": "$.shelly_id",
    "collectionPrefix": "shelly_",
    "timestampPath": null,
    "fieldsMapping": {
        "a_current": "$.a_current",
        "b_current": "$.b_current",
        "c_current": "$.c_current",
        "total_current": "$.total_current",
        "a_act_power": "$.a_act_power",
        "b_act_power": "$.b_act_power",
        "c_act_power": "$.c_act_power",
        "total_act_power": "$.total_act_power",
        "a_aprt_power": "$.a_aprt_power",
        "b_aprt_power": "$.b_aprt_power",
        "c_aprt_power": "$.c_aprt_power",
        "total_aprt_power": "$.total_aprt_power",
        "a_freq": "$.a_freq",
        "b_freq": "$.b_freq",
        "c_freq": "$.c_freq",
        "a_pf": "$.a_pf",
        "b_pf": "$.b_pf",
        "c_pf": "$.c_pf",
        "a_voltage": "$.a_voltage",
        "b_voltage": "$.b_voltage",
        "c_voltage": "$.c_voltage"
    }
}
```
### Body du endpoint Energy
```
{
    "name": "Energy",
    "collectionName": "$.shelly_id",
    "collectionPrefix": "shelly_",
    "timestampPath": null,
    "fieldsMapping": {
        "a_total_act_energy": "$.a_total_act_energy",
        "a_total_act_ret_energy": "$.a_total_act_ret_energy",
        "b_total_act_energy": "$.b_total_act_energy",
        "b_total_act_ret_energy": "$.b_total_act_ret_energy",
        "c_total_act_energy": "$.c_total_act_energy",
        "c_total_act_ret_energy": "$.c_total_act_ret_energy",
        "total_act": "$.total_act",
        "total_act_ret": "$.total_act_ret"
    }
}
```
## Test de fiabilité du Shelly
Les mesures de comparaison ont été faite avec un LMG450. Les détails de celles-ci se trouvent dans le fichier `doc/shelly_mesures.xlxs`.
Voici les résultats des mesures:

|                | Mean Percentage Error |
|----------------|-----------------------|
| Current        | 2,37%                 |
| Active Power   | 0,38%                 |
| Apparent Power | 0,21%                 |
| Voltage        | 0,11%                 |