#!/bin/bash

# Check if an argument is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <id>"
    exit 1
fi

# Get the input ID
ID=$1

# Generate the JSON with the provided ID
cat <<EOF > dashboard.txt
{
    "name": "Shelly $ID",
    "layout": {
        "items": [
            {
                "x": 0,
                "y": 0,
                "cols": 2,
                "rows": 3,
                "widget": {
                    "type": "line",
                    "title": "Active Power",
                    "queries": [
                        {
                            "unit": "W",
                            "query": "shelly_$ID.a_act_power"
                        },
                        {
                            "unit": "W",
                            "query": "shelly_$ID.b_act_power"
                        },
                        {
                            "unit": "W",
                            "query": "shelly_$ID.c_act_power"
                        },
                        {
                            "unit": "W",
                            "query": "shelly_$ID.total_act_power"
                        }
                    ],
                    "rangeMode": "global"
                },
                "maxItemCols": 10,
                "maxItemRows": 10
            },
            {
                "x": 2,
                "y": 0,
                "cols": 2,
                "rows": 3,
                "widget": {
                    "type": "line",
                    "title": "Apparent Power",
                    "queries": [
                        {
                            "unit": "VA",
                            "query": "shelly_$ID.a_aprt_power"
                        },
                        {
                            "unit": "VA",
                            "query": "shelly_$ID.b_aprt_power"
                        },
                        {
                            "unit": "VA",
                            "query": "shelly_$ID.c_aprt_power"
                        },
                        {
                            "unit": "VA",
                            "query": "shelly_$ID.total_aprt_power"
                        }
                    ],
                    "rangeMode": "global"
                },
                "maxItemCols": 10,
                "maxItemRows": 10
            },
            {
                "x": 0,
                "y": 3,
                "cols": 2,
                "rows": 3,
                "widget": {
                    "type": "line",
                    "title": "Current",
                    "queries": [
                        {
                            "unit": "A",
                            "query": "shelly_$ID.a_current"
                        },
                        {
                            "unit": "A",
                            "query": "shelly_$ID.b_current"
                        },
                        {
                            "unit": "A",
                            "query": "shelly_$ID.c_current"
                        },
                        {
                            "unit": "A",
                            "query": "shelly_$ID.total_current"
                        }
                    ],
                    "rangeMode": "global"
                },
                "maxItemCols": 10,
                "maxItemRows": 10
            },
            {
                "x": 2,
                "y": 3,
                "cols": 2,
                "rows": 3,
                "widget": {
                    "type": "line",
                    "title": "Power Factor",
                    "queries": [
                        {
                            "query": "shelly_$ID.a_pf"
                        },
                        {
                            "query": "shelly_$ID.b_pf"
                        },
                        {
                            "query": "shelly_$ID.c_pf"
                        }
                    ],
                    "rangeMode": "global"
                },
                "maxItemCols": 10,
                "maxItemRows": 10
            },
            {
                "x": 4,
                "y": 0,
                "cols": 1,
                "rows": 2,
                "widget": {
                    "type": "gauge",
                    "title": "Phase 1 Voltage",
                    "maximum": 500,
                    "minimum": 0,
                    "queries": [
                        {
                            "unit": "V",
                            "query": "shelly_$ID.a_voltage"
                        }
                    ],
                    "rangeMode": "global"
                },
                "maxItemCols": 10,
                "maxItemRows": 10
            },
            {
                "x": 4,
                "y": 2,
                "cols": 1,
                "rows": 2,
                "widget": {
                    "type": "gauge",
                    "title": "Phase 2 Voltage",
                    "maximum": 500,
                    "minimum": 0,
                    "queries": [
                        {
                            "unit": "V",
                            "query": "shelly_$ID.b_voltage"
                        }
                    ],
                    "rangeMode": "global"
                },
                "maxItemCols": 10,
                "maxItemRows": 10
            },
            {
                "x": 4,
                "y": 4,
                "cols": 1,
                "rows": 2,
                "widget": {
                    "type": "gauge",
                    "title": "Phase 3 Voltage",
                    "maximum": 500,
                    "minimum": 0,
                    "queries": [
                        {
                            "unit": "V",
                            "query": "shelly_$ID.c_voltage"
                        }
                    ],
                    "rangeMode": "global"
                },
                "maxItemCols": 10,
                "maxItemRows": 10
            },
            {
                "x": 5,
                "y": 0,
                "cols": 2,
                "rows": 3,
                "widget": {
                    "type": "line",
                    "title": "Active Energy",
                    "queries": [
                        {
                            "unit": "kWh",
                            "query": "shelly_$ID.a_total_act_energy"
                        },
                        {
                            "unit": "kWh",
                            "query": "shelly_$ID.b_total_act_energy"
                        },
                        {
                            "unit": "kWh",
                            "query": "shelly_$ID.c_total_act_energy"
                        },
                        {
                            "unit": "kWh",
                            "query": "shelly_$ID.total_act"
                        }
                    ],
                    "rangeMode": "global"
                },
                "maxItemCols": 10,
                "maxItemRows": 10
            },
            {
                "x": 5,
                "y": 3,
                "cols": 2,
                "rows": 3,
                "widget": {
                    "type": "line",
                    "title": "Reactive Energy",
                    "queries": [
                        {
                            "unit": "VARh",
                            "query": "shelly_$ID.a_total_act_ret_energy"
                        },
                        {
                            "unit": "VARh",
                            "query": "shelly_$ID.b_total_act_ret_energy"
                        },
                        {
                            "unit": "VARh",
                            "query": "shelly_$ID.c_total_act_ret_energy"
                        },
                        {
                            "unit": "VARh",
                            "query": "shelly_$ID.total_act_ret"
                        }
                    ],
                    "rangeMode": "global"
                },
                "maxItemCols": 10,
                "maxItemRows": 10
            },
            {
                "x": 0,
                "y": 6,
                "cols": 1,
                "rows": 2,
                "widget": {
                    "type": "gauge",
                    "title": "Phase 1 Frequency",
                    "maximum": 70,
                    "minimum": 0,
                    "queries": [
                        {
                            "unit": "Hz",
                            "query": "shelly_$ID.a_freq"
                        }
                    ],
                    "rangeMode": "global"
                },
                "maxItemCols": 10,
                "maxItemRows": 10
            },
            {
                "x": 1,
                "y": 6,
                "cols": 1,
                "rows": 2,
                "widget": {
                    "type": "gauge",
                    "title": "Phase 2 Frequency",
                    "maximum": 70,
                    "minimum": 0,
                    "queries": [
                        {
                            "unit": "Hz",
                            "query": "shelly_$ID.b_freq"
                        }
                    ],
                    "rangeMode": "global"
                },
                "maxItemCols": 10,
                "maxItemRows": 10
            },
            {
                "x": 2,
                "y": 6,
                "cols": 1,
                "rows": 2,
                "widget": {
                    "type": "gauge",
                    "title": "Phase 3 Frequency",
                    "maximum": 70,
                    "minimum": 0,
                    "queries": [
                        {
                            "unit": "Hz",
                            "query": "shelly_$ID.c_freq"
                        }
                    ],
                    "rangeMode": "global"
                },
                "maxItemCols": 10,
                "maxItemRows": 10
            }
        ]
    },
    "timeRange": 3600,
    "refreshInterval": 10,
    "home": false
}
EOF
