let CONFIG = {
    SHELLY_ID: 1,
    LYSR_PUSH_POWER_URL: "xxx",
    LYSR_PUSH_ENERGY_URL: "xxx"
};

function shelly_post(url, body) {
    Shelly.call(
        'HTTP.POST',
        {url: url, body: body},
        function (res, error_code, error_msg, ud) {
            if (error_code !== 0) {
                console.log("Error making HTTP request:", error_code, error_msg);
                return;
            }

            if (res.code >= 200 && res.code < 300) {
                console.log("Success push");
            } else {
                console.log("Request failed with status code:", res.code);
            }
        },
        null
    );
}

function push_data(data) {
    try {
        let body = data.delta;
        delete body.id;
        body.shelly_id = CONFIG.SHELLY_ID.toString();
        if (data.name === "em") {
            shelly_post(CONFIG.LYSR_PUSH_POWER_URL, body);
        } else if (data.name === "emdata") {
            shelly_post(CONFIG.LYSR_PUSH_ENERGY_URL, body);
        }
    } catch (e) {
        console.log("Error:", e)
    }
}

Shelly.addStatusHandler(push_data);


